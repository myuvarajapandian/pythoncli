class Argument:
    def __init__(self, args):
        self.command = []
        self.options = []
        self.optionsValues = {} # Dict/Set here... Unique Index/NO Repeating Value
        self.args = args
        print(self.args)
        
        for arg in self.args:
            if '-' in arg:
                if '=' in arg:
                    # this is option with value
                    pair = arg.split('=')
                    self.optionsValues[pair[0]] = pair[1] #appending the key value pair
                    self.options.append(pair[0])
                else:
                    # this is just an option
                    self.options.append(arg)
            else:
                self.command.append(arg)
           
    def hasOptions(self, options: list):
        useroptions = set(self.options)
        reqoptions = set(options)
        return list(reqoptions & useroptions)
    
    def hasOption(self, option):
        return option in self.hasOptions([option])
    
    def hasOptionValue(self, option):
        return option in self.optionsValues
    
    def hasCommands(self, commands):
        usercommands = set(self.command)
        reqcommands = set(commands)
        return list(reqcommands & usercommands)
    
    def hasCommand(self, command):
        return command in self.hasCommands([command])
    
    def getOptionValue(self, option, default=None):
        if option in self.optionsValues:
            return self.optionsValues[option]
        else:
            return default